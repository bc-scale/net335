# NET335 re:Invent 2019

## Cloud Networking for the On-prem Network Guru

Find out how to translate the on-premises networking world into the cloud language. Embrace the culture of DevOps and infrastructure as code to speed up your network operations. Discover how to avoid pitfalls with centralized firewall deployments in the cloud.

